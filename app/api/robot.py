import os
import re
from flask import current_app
from flask_restplus import abort, Resource
from .resources import ProtectedResource
from .namespaces import ns_robot
from ..models.robot import Robot
from ..schemas import RobotSchema


@ns_robot.route('', endpoint='robots')
class RobotListAPI(Resource):
    def get(self):
        """List Robot"""
        schema = RobotSchema(many=True)
        response, errors = schema.dump(Robot.select())
        if errors:
            abort(409, errors)
        return response

    @ns_robot.expect(RobotSchema.fields())
    def post(self):
        schema = RobotSchema()
        robot, errors = schema.load(current_app.api.payload)
        if errors:
            abort(409, errors)
        robot.save()
        return schema.dump(robot)


@ns_robot.route('/<id>', endpoint='robot')
@ns_robot.response(404, 'Robot not found')
class RobotAPI(ProtectedResource):
    def get(self, id):
        """Get robot details"""
        try:
            robot = Robot.get(id=id)
        except Robot.DoesNotExist:
            abort(404, 'Robot not found')
        schema = RobotSchema()
        response, errors = schema.dump(robot)
        if errors:
            abort(409, errors)
        return response
