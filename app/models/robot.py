from peewee import TextField
from ..db import db


Model = db.Model


class Robot(Model):
    serial = TextField()
